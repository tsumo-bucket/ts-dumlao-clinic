<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="format-detection" content="telephone=no"> <!-- Disable Safari phone number detection that transform text to link  -->

  <title>{!! Seo::title(@$seo['title']) !!}</title>
  <meta name="description" content="{!! Seo::description(@$seo['description']) !!}">
  <meta name="csrf-token" content="{{ csrf_token() }}" />

  {!! Html::style('css/app-import.css') !!}
  {!! Html::style('css/app.css') !!}

</head>
<!-- GA code is configured with CONFIG options -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', '{!! General::get_ga_code() !!}', 'auto');
  ga('send', 'pageview');
</script> 
<body>
  <nav class="navbar navbar-expand-md navbar-dark bg-dark fixed-top">
    <a class="navbar-brand" href="{{ route('homePage') }}">Think Big</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarsExampleDefault">
      <ul class="navbar-nav mr-auto">
        <li class="nav-item active">
          <a class="nav-link" href="{{ route('homePage') }}">Home</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="{{ route('homePage') }}">Link</a>
        </li>
        <li class="nav-item">
          <a class="nav-link disabled" href="#">Disabled</a>
        </li>
        <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle" href="#" id="dropdown01" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Dropdown</a>
          <div class="dropdown-menu" aria-labelledby="dropdown01">
            <a class="dropdown-item" href="{{ route('homePage') }}">Action</a>
            <a class="dropdown-item" href="{{ route('homePage') }}">Another action</a>
            <a class="dropdown-item" href="{{ route('homePage') }}">Something else here</a>
          </div>
        </li>
      </ul>
      <form action="{{ route('homePage') }}" method="get" class="form-inline my-2 my-lg-0">
        <input class="form-control mr-sm-2" type="text" placeholder="Keyword" aria-label="Keyword" name="keyword">
        <button class="btn btn-outline-info my-2 my-sm-0" type="submit">Search</button>
      </form>
    </div>
  </nav>
  <example-component></example-component>

  @yield('content')

  @include('admin.templates.asset_image')
  {!! Html::script('js/all.js') !!}
  {!! Html::script('js/app.js') !!}
  <script type="text/javascript">
    $.ajaxSetup({
        beforeSend: function(xhr,data) {
          xhr.setRequestHeader('X-CSRF-TOKEN', $('meta[name="csrf-token"]').attr('content'));
        }
    });
  </script>
  @if(Session::has('message'))
    <script type="text/javascript">
      swal({title: "",
            text:"Oops... {{ Session::get('message') }}",
            type: "error",
            confirmButtonText: "OK",
            closeOnConfirm: false,
            showCancelButton: false
          },
          function(){
            location.reload();
      });
    </script>
  @endif
  @yield('added-scripts')
</body>
</html>
